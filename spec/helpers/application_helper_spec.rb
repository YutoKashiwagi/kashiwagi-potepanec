require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper
  describe 'full_titleメソッドのテスト' do
    let(:product) { create(:product) }

    it 'トップページのタイトル' do
      expect(full_title("")).to eq 'BIGBAG'
    end

    it '商品ページのタイトル' do
      expect(full_title(product.name)).to eq "#{product.name} | BIGBAG"
    end
  end
end
