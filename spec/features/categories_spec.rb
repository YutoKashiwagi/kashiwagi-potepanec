require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let!(:categories) { create(:taxonomy, name: 'Categories') }
  let!(:bag) { create(:taxon, taxonomy: categories, name: 'Bag', parent_id: categories.root.id) }
  let!(:tote) { create(:product, name: 'tote', taxons: [bag]) }
  let!(:mug) { create(:taxon, taxonomy: categories, name: 'Mug', parent_id: categories.root.id) }
  let!(:ruby_mug) { create(:product, name: 'ruby_mug', taxons: [mug]) }

  before { visit potepan_category_path(bag.id) }

  describe '商品一覧部分' do
    it 'bagカテゴリーの商品(tote)が正しく表示されていること' do
      expect(page).to have_content tote.name
      expect(page).to have_content tote.display_price
    end

    it '商品個別ページへのリンクが機能していること' do
      click_link tote.display_price
      expect(current_path).to eq potepan_product_path(tote.id)
    end
  end

  describe 'サイドバー（カテゴリー）' do
    before do
      within('.side-nav') do
        click_on categories.name
      end
    end

    it 'プルダウンが機能していること' do
      within('.side-nav') do
        expect(page).to have_content bag.name
        expect(page).to have_content bag.all_products.count
      end
    end

    it 'カテゴリー別のリンクが機能していること' do
      within('.side-nav') { click_link mug.name }
      expect(current_path).to eq potepan_category_path(mug.id)
      expect(page).to have_content ruby_mug.name
      expect(page).not_to have_content tote.name
    end
  end
end
