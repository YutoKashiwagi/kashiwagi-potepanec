require 'rails_helper'

RSpec.feature "Products", type: :feature do
  given!(:categories) { create(:taxonomy, name: 'Categories') }
  given!(:bag) { create(:taxon, taxonomy: categories, name: 'Bag') }
  given!(:tote) { create(:product, name: 'tote', taxons: [bag]) }

  feature 'メイン部分' do
    background { visit potepan_product_path(tote.id) }
    scenario '正しい商品情報が表示されていること' do
      expect(page).to have_content tote.name
      expect(page).to have_selector 'h3', text: tote.price
      expect(page).to have_selector 'p', text: tote.description
    end

    scenario 'トップページへのリンク' do
      find('.link_to_potepan_path_light_section').click
      expect(current_path).to eq potepan_path
    end

    scenario 'カテゴリー一覧ページへのリンク' do
      click_link '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(bag.id)
    end
  end

  feature '関連商品部分' do
    given!(:ruby_tote) { create(:product, name: 'ruby_tote', taxons: [bag]) }
    given!(:mug) { create(:taxon, taxonomy: categories, name: 'Mug') }
    given!(:rails_mug) { create(:product, name: 'rails_mug', taxons: [mug]) }

    background { visit potepan_product_path(tote.id) }

    scenario '関連商品が表示されていること' do
      within('.productsContent') do
        expect(page).to have_content ruby_tote.name
        expect(page).to have_content ruby_tote.display_price
      end
    end

    scenario '関連商品のリンクが機能していること' do
      click_on ruby_tote.name
      expect(current_path).to eq potepan_product_path(ruby_tote.id)
    end

    context '関連商品が4つ以上ある場合' do
      background do
        create_list(:product, 4, taxons: [bag])
        visit potepan_product_path(tote.id)
      end
      scenario '関連商品は最大4つまでしか表示されないこと' do
        expect(bag.all_products.count).to eq 6
        within('.productsContent') do
          expect(page).to have_selector '.productBox', count: 4
        end
      end
    end

    scenario '別カテゴリの商品が表示されていないこと' do
      within('.productsContent') do
        expect(page).not_to have_content rails_mug.name
      end
    end
  end
end
