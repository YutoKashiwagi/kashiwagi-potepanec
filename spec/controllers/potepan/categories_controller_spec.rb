require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe '#show' do
    let(:categories) { create(:taxonomy, name: 'Categories') }
    let(:bag) { create(:taxon, taxonomy: categories, name: 'Bag') }
    let(:tote) { create(:product, name: 'tote', taxons: [bag]) }

    before { get :show, params: { id: bag.id } }

    it '正常なレスポンスを返すこと' do
      expect(response).to be_successful
    end

    it '正しいページが表示されていること' do
      expect(response).to render_template :show
    end

    it 'インスタンス変数が正しいこと' do
      expect(assigns(:taxon)).to eq bag
    end

    describe 'partial' do
      render_views
      subject { response }

      it 'top_light_section' do
        is_expected.to render_template(partial: '_top_light_section')
      end

      it 'sidebar_categories' do
        is_expected.to render_template(partial: '_sidebar_categories')
      end

      it 'filter_area' do
        is_expected.to render_template(partial: '_filter_area')
      end

      it 'condition_sort' do
        is_expected.to render_template(partial: '_condition_sort')
      end

      it 'bottom_light_section' do
        is_expected.to render_template(partial: '_bottom_light_section')
      end
    end
  end
end
