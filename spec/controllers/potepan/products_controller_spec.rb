require 'rails_helper'
require 'potepan/products_controller'

RSpec.describe Potepan::ProductsController, type: :controller do
  let(:categories) { create(:taxonomy, name: 'Categories') }
  let(:bag) { create(:taxon, taxonomy: categories, name: 'Bag') }
  let(:tote) { create(:product, name: 'tote', taxons: [bag]) }

  describe '#show' do
    before { get :show, params: { id: tote.id } }

    it '正常なレスポンスを返すこと' do
      expect(response).to be_successful
    end

    it '正しいページが表示されていること' do
      expect(response).to render_template :show
    end

    it '@productが正しいこと' do
      expect(assigns(:product)).to eq tote
    end

    describe 'partial' do
      render_views
      it 'header' do
        expect(response).to render_template(partial: 'shared/_header')
      end

      it 'footer' do
        expect(response).to render_template(partial: 'shared/_footer')
      end

      it 'related_products' do
        expect(response).to render_template(partial: '_related_products')
      end
    end
  end
end
